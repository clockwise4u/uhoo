<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function(){

Route::get('dashboard', 'HomeController@index');
	/*
	|-------------------------------------------------------------
	|		INTEREST related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'interest'], function(){
		
		Route::get('list', 'InterestController@list');
		
		Route::get('/', 'InterestController@index');

		Route::post('/', 'InterestController@store');
		
		// Route::get('{uid}', 'InterestController@show');
		
		// Route::get('{uid}/edit', 'InterestController@edit');
		
		Route::patch('{uid}', 'InterestController@update');
		
		Route::delete('{uid}', 'InterestController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		TOPICS related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'topics'], function(){
		
		Route::get('list', 'TopicController@list');
		
		Route::get('/', 'TopicController@index');

		Route::post('/', 'TopicController@store');
		
		// Route::get('{uid}', 'TopicController@show');
		
		// Route::get('{uid}/edit', 'TopicController@edit');
		
		Route::patch('{uid}', 'TopicController@update');
		
		Route::delete('{uid}', 'TopicController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		B2B related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'b2b'], function(){
		
		Route::get('list', 'B2BController@list');
		
		Route::get('/', 'B2BController@index');

		Route::post('/', 'B2BController@store');
		
		// Route::get('{uid}', 'B2BController@show');
		
		// Route::get('{uid}/edit', 'B2BController@edit');
		
		Route::patch('{uid}', 'B2BController@update');
		
		Route::delete('{uid}', 'B2BController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		B2C related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'b2c'], function(){
		
		Route::get('list', 'B2CController@list');
		
		Route::get('/', 'B2CController@index');

		Route::post('/', 'B2CController@store');
		
		// Route::get('{uid}', 'B2CController@show');
		
		// Route::get('{uid}/edit', 'B2CController@edit');
		
		Route::patch('{uid}', 'B2CController@update');
		
		Route::delete('{uid}', 'B2CController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		OFFER related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'offers'], function(){
		
		Route::get('list', 'OfferController@list');
		
		Route::get('/', 'OfferController@index');

		Route::post('/', 'OfferController@store');
		
		// Route::get('{uid}', 'OfferController@show');
		
		// Route::get('{uid}/edit', 'OfferController@edit');
		
		Route::patch('{uid}', 'OfferController@update');
		
		Route::delete('{uid}', 'OfferController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		EVENT related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'events'], function(){
		
		Route::get('list', 'EventController@list');
		
		Route::get('/', 'EventController@index');

		Route::post('/', 'EventController@store');
		
		// Route::get('{uid}', 'EventController@show');
		
		// Route::get('{uid}/edit', 'EventController@edit');
		
		Route::patch('{uid}', 'EventController@update');
		
		Route::delete('{uid}', 'EventController@destroy');
	});

	/*
	|-------------------------------------------------------------
	|		FAQS related ROUTES 
	|-------------------------------------------------------------
	*/
	Route::group(['prefix' => 'faqs'], function(){
		
		Route::get('list', 'FaqsController@list');
		
		Route::get('/', 'FaqsController@index');

		Route::post('/', 'FaqsController@store');
		
		// Route::get('{uid}', 'FaqsController@show');
		
		// Route::get('{uid}/edit', 'FaqsController@edit');
		
		Route::patch('{uid}', 'FaqsController@update');
		
		Route::delete('{uid}', 'FaqsController@destroy');
	});


});
