<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function(){

	// Route::prefix('v1')->group(function(){
		
	Route::post('login', 'AuthController@login');

	Route::post('register', 'AuthController@register');
	
	Route::post('forgotPassword', 'AuthController@forgot_password');

	Route::post('uploadProfilePic', 'CustomerController@upload_profile_image');


	// Routes After login
	Route::group(['middleware' => 'auth:api'], function(){

		Route::get('getLoggedinUser', 'AuthController@getLoggedinUser');
		
		// get All Interest
		Route::get('getInterest', 'CommonController@get_interest');
		
		// get All Topics
		Route::get('getTopics', 'CommonController@get_topics');

		// Update Profile Detail
		Route::post('completeProfile', 'CustomerController@complete_profile');

		// Upload Profile Image
		// Route::post('uploadProfilePic', 'CustomerController@upload_profile_image');

		// Update Location
		Route::post('updateLocation', 'CommonController@update_location');
	});
});
