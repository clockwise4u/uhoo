<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_locations';

	protected $fillable = [

        'unique_id',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'country',
        'postal_code',
        'map_zoom',
        'longitude',
        'latitude',
    ];

    protected $dates = ['deleted_at'];
}