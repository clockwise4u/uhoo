<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_offers';

	protected $fillable = [

        'unique_id',
        'business_id',
        'offer_name',
        'offer_code',
        'offer_type',
        'description',
        'start_date',
        'end_date',
        'location_id',
    ];

    protected $dates = ['deleted_at'];
}