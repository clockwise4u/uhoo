<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_customers';

	protected $fillable = [

        'user_id',
        'dob',
        'title',
        'description',
        'interests',
        'location_id',
    ];

    protected $dates = ['deleted_at'];
}