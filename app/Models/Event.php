<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_offers';

	protected $fillable = [

        'unique_id',
        'business_id',
        'event_name',
        'event_type',
        'description',
        'start_date',
        'end_date',
        'location_id',
    ];

    protected $dates = ['deleted_at'];
}