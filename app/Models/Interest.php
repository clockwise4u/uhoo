<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_interests';

	protected $fillable = [

        'unique_id',
        'interest_name',
    ];

    protected $dates = ['deleted_at'];
}