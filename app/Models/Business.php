<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_business';

	protected $fillable = [

        'user_id',
        'restaurant_name',
        'location_id',
        'tags',
        'description',
        'shop_license',
        'pan',
    ];

    protected $dates = ['deleted_at'];
}