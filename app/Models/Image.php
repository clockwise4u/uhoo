<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_images';

	protected $fillable = [

        // 'unique_id',
        'type', // 1=topics, 2=business_logo, 3=business_slider, 4=customer_profile
        'type_id',
        'filename',
        'original_name',
    ];

    protected $dates = ['deleted_at'];

    public function getFilenameAttribute($value)
    {
        return url('storage/app/'.$value);
    }
}