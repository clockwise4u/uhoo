<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_topics';

	protected $fillable = [

        'unique_id',
        'topic_name',
    ];

    protected $dates = ['deleted_at'];
}