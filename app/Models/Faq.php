<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use SoftDeletes;
    
    protected $table = 'tbl_faqs';

	protected $fillable = [

        'unique_id',
        'question',
        'answer',
    ];

    protected $dates = ['deleted_at'];
}