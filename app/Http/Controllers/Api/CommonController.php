<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\BackendController;
use App\User; 
use App\Models\Customer;
use App\Models\Business;
use App\Models\Location;
use App\Models\Interest;
use App\Models\Topic;
use DB;
// use App\Mail\MailForgotPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth; 
use Validator;
class CommonController extends BackendController 
{
    public $successStatus = 200;

    public function get_interest() {    
        
        $all_interest = Interest::all();
        
        return $this->setResponseFormat($this->successStatus, "List of all Interest", $all_interest);
    }

    public function get_topics() {    
        
        $all_topics = DB::table('tbl_topics as tt')
        // ->join('tbl_images as ti', "tt.id", "=", "ti.type_id")
        ->join('tbl_images as ti', function($join)
        {
            $join->on("tt.id", "=", "ti.type_id")
                ->where("ti.type", "=", 1)
                ->whereNull('ti.deleted_at');
        })
        ->select([
            'tt.unique_id',
            'tt.topic_name',
            'tt.is_active',
            'ti.filename',
            'tt.is_active',
        ])
        ->whereNull("tt.deleted_at")
        ->whereNull("ti.deleted_at")
        ->get();
        
        return $this->setResponseFormat($this->successStatus, "List of all Topics", $all_topics);
    }

    public function update_location(Request $request) {    
        

        $user = $request->user();

        $input = $request->all();

        $location_input = array(

            'unique_id'         => md5(time().time()),
            'address_line_1'    => $input['address_line_1'],
            'address_line_2'    => isset($input['address_line_2']) ? $input['address_line_2'] : "",
            'city'              => $input['city'],
            'state'             => $input['state'],
            'country'           => $input['country'],
            'postal_code'       => $input['postal_code'],
            'map_zoom'          => isset($input['map_zoom']) ? $input['map_zoom'] : "1",
            'longitude'         => $input['longitude'],
            'latitude'          => $input['latitude'],
        );

        $create_location = Location::create($location_input);

        if($user->user_type == 1)
        {
            $loc_upd = Customer::where("user_id", "=", $user->id)->update(['location_id' => $create_location->id]);
        }
        elseif($user->user_type == 2)
        {
            $loc_upd = Business::where("user_id", "=", $user->id)->update(['location_id' => $create_location->id]);
        }

        /*if($loc_upd)
        {
            $profile_upd = Customer::where("user_id", "=", $user->id)->update($profile_input);
        }
        else
        {
            $profile_input['user_id'] = $user->id;
            $profile_upd = Customer::create($profile_input);
        }*/

        if($loc_upd)
        {
            return $this->setResponseFormat(200, "Your Location has been successfully updated", "", 1);
        }
        else
        {
            return $this->setResponseFormat(200, "Sorry! Unable to update the location this time. Please try again later.", "", 0);
        }
    }
} 