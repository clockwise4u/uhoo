<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\BackendController;
use App\User; 
use App\Models\Customer;
use App\Mail\MailForgotPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth; 
use Validator;
class CustomerController extends BackendController 
{
    public $successStatus = 200;

    public function complete_profile(Request $request) {    
        
        $user = $request->user();
        
        // return $this->setResponseFormat(200, "USer Details.!", $user->id, 1);
        
        $validator = Validator::make($request->all(), [ 
            'title'       => 'required',
            'description' => 'required',
            'interests'   => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->setResponseFormat(401, "Error Occured.!", $validator->errors(), 0);
            // return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

        $profile_input = array(
            'title'       => $input['title'],
            'description' => $input['description'],
            'interests'   => json_encode($input['interests']),
        );

        $cnt_dtl = Customer::where("user_id", "=", $user->id)->count();
        if($cnt_dtl > 0)
        {
            $profile_upd = Customer::where("user_id", "=", $user->id)->update($profile_input);
        }
        else
        {
            $profile_input['user_id'] = $user->id;
            $profile_upd = Customer::create($profile_input);
        }

        if(($cnt_dtl > 0 && $profile_upd) || ($cnt_dtl <= 0 && @$profile_upd->id && $profile_upd->id != ''))
        {
            return $this->setResponseFormat(200, "Your profile has been successfully updated", "", 1);
        }
        else
        {
            return $this->setResponseFormat(200, "Sorry! Unable to update the profile this time. Please try again later.", "", 0);
        }
    }

    public function upload_profile_image(Request $request){ 

        /*$user_id = $request->user()->id
        $profile_image_data = array(

            'type' => 4,
            'type_id' => $user_id,
            'filename' => "testing",
            'original_name' => $request->profile_image,
        );

        $added_img = Image::create($profile_image_data);*/

        if($request->hasFile('profile_image'))
        {
        
            $upld_actual_site_v_slider_image = $request->file('profile_image')->store('profile_image');

            chmod('storage/app/'.$upld_actual_site_v_slider_image, 0777);

            /*if($upld_actual_site_v_slider_image != '')
            {
                $actual_site_slider = $upld_actual_site_v_slider_image;
            }
            else
            {
                $actual_site_slider = null;
            }*/

            /*$cropped_image_name = explode('/', $upld_actual_site_v_slider_image);
            $cropped_image_name = end($cropped_image_name);

                $cropped_image = $request->crop_image_val;  // your base64 encoded

                $image_array_1 = explode(";", $cropped_image);
                $image_array_2 = explode(",", $image_array_1[1]);
                
                $dir_path = storage_path(). '/app/actual_site_view_slider/thumbnails/';

                if(!\File::exists($dir_path))
                {
                    \File::makeDirectory($dir_path, $mode = 0777, true, true);
                }

                \File::put(storage_path(). '/app/actual_site_view_slider/thumbnails/' . $cropped_image_name, base64_decode($image_array_2[1]));

                chmod('storage/app/actual_site_view_slider/thumbnails/'.$cropped_image_name, 0777);
                $actual_site_slider = 'actual_site_view_slider/thumbnails/' . $cropped_image_name;

            }
            else
            {
                $actual_site_slider = null;
            }*/

            /*$actual_site_view_sliders_detail['image'] = $actual_site_slider;
            
            $asmslider = ActualSiteSlider::create($actual_site_view_sliders_detail);

            if(@$asmslider->id){

                $return = array(
                    'message' => "Actual Site View Slider image and PDF Added Successfully" , 
                    'title' => "Done", 
                    'type' => "success", 
                    );

                return response()->json($return);
            }
            else
            {
             $return = array(
                'message' => "Unable to add Actual Site View Detail. Try again later.." , 
                'title' => "Sorry", 
                'type' => "error", 
                );

             return response()->json($return);*/
            return $this->setResponseFormat(200, "To check the Image Format", $upld_actual_site_v_slider_image, 1);
        }
        else
        {
            return $this->setResponseFormat(200, "Nothing Happened", "", 0);

        }
    }

    /*public function getLoggedinUser() {
        
        
    }*/

    public function forgot_password(Request $request) {
        
        $email = $request->email;

        $email_exists = User::where('email', '=', $email)->count();
        $user_dtl = User::select(['name', 'email'])->where('email', '=', $email)->first();
        
        if($email != "" && $email_exists > 0)
        {
            $new_password = mt_rand(11111111, 99999999);

            $pass_upd = User::where('email', '=', $email)->update(['password' => bcrypt($new_password)]);

            if($pass_upd) // MailForgotPassword
            {
                $usr_dtl['password'] = $new_password;
                $usr_dtl['email'] = $email;
                $usr_dtl['name'] = $user_dtl->email;
                // echo $user_dtl->password; exit;
                // Mail::to($email)->send(new MailForgotPassword($user_dtl));

                Mail::send('mails.forgot-password', $usr_dtl, function($message) use ($usr_dtl){
 
                    $message->to($usr_dtl['email'], ucwords($usr_dtl['name']))
         
                            ->subject('Recover Forgot Password');
                });

                if(count(Mail::failures()) > 0)
                {
                    return $this->setResponseFormat(400, "Failed to send password on your email, Please try again.", Mail::failures(), 0);
                }
                else
                {
                    return $this->setResponseFormat($this->successStatus, "Password has been sent successfully to your email ".$email, "");
                }
            }
        }
        else
        {
            if($email == "")
            {
                return $this->setResponseFormat(400, "Please enter the email first.", "", 0);
            }
            else
            {
                return $this->setResponseFormat(400, "Your are not registered with us. Please register first", "", 0);
            }
        }
    }
} 