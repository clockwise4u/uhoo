<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Response, Route;
// use Hashids\Hashids;

class BackendController extends Controller
{
    // protected $hashids;

    public function __construct(){
        // $this->hashids = new Hashids(env('APP_KEY'), 10);
        // if(Route::current()->hasParameter('id')){
        //     $id = Route::current()->parameter('id');
        //     Route::current()->setParameter('id', $this->hashids->decode($id)[0]);
        // }
    }

    protected $resultFormat = array(
            'message'      => ':message',
            'data' 	       => ':data',
            'status'        => ':status',
            'code'         => ':code',
    	);

    protected function setResponseFormat($code = 200, $message, $data = NULL, $status = 1){
    	$this->resultFormat = [
            'code'     => $code,
    		'data'	   => $data,
    		'message'  => $message,
            'status'   => $status,
    	];

    	return Response::json($this->resultFormat, $code);
            // ->header('Access-Control-Allow-Headers', '*')
            // ->header('Access-Control-Allow-Origin', '*')
            // ->header('Access-Control-Allow-Methods', '*')
            // ->header('Content-Type', 'application/json')
            // ->header('Access-Control-Allow-Credentials', true);
            // ->header('HTTP_Authorization', 'Bearer ' . $token);
    }
}
