<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\BackendController;
use App\User; 
use App\Models\Customer;
use App\Models\Business;
use App\Mail\MailForgotPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth; 
use Validator;
class AuthController extends BackendController 
{
    public $successStatus = 200;

    public function register(Request $request) {    
        
        $validator = Validator::make($request->all(), [ 
            'name'       => 'required',
            'email'      => 'required|email|unique:users',
            'phone'      => 'required|numeric|unique:users',
            'dob'        => 'required',
            'password'   => 'required',
            'c_password' => 'required|same:password',
        ]);   
        if ($validator->fails())
        {
            return $this->setResponseFormat(401, "Error Occured.!", $validator->errors(), 0);
            // return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $user_input = array(
            'name'       => $input['name'],
            'email'      => $input['email'],
            'phone'      => $input['phone'],
            'password'   => bcrypt($input['password']),
            'user_type'  => $input['user_type'],
            'gender'     => $input['gender'],
        );
        // $input['password'] = bcrypt($input['password']);
        $user = User::create($user_input);

        if($input['dob'] != "")
        {
            if($input['user_type'] == '1')
            {
                $count = Customer::where('user_id', '=', $user->id)->count();

                if($count <= 0)
                {
                    $customer_detail = array(
                        'dob' => date('Y-m-d', strtotime($input['dob'])),
                        'user_id' => $user->id,
                    );
                    Customer::create($customer_detail);
                }
                else
                {
                    $customer_detail = array(
                        'dob' => date('Y-m-d', strtotime($input['dob'])),
                    );
                    Customer::where('user_id', '=', $user->id)->update($customer_detail);
                }
            }
            elseif($input['user_type'] == '2')
            {
                $count = Business::where('user_id', '=', $user->id)->count();

                if($count <= 0)
                {
                    $business_detail = array(

                        'dob' => date('Y-m-d', strtotime($input['dob'])),
                        'user_id' => $user->id,
                    );
                    Business::create($business_detail);
                }
                else
                {
                    $business_detail = array(

                        'dob' => date('Y-m-d', strtotime($input['dob'])),
                    );
                    Business::where('user_id', '=', $user->id)->update($customer_detail);
                }
            }
        }

        $success['token'] =  $user->createToken('appName')->accessToken;
        $success['image_url'] =  url('storage/app').'/';

        User::where("id", "=", $user->id)->update(["api_token" => $success['token']]);
        
        return $this->setResponseFormat($this->successStatus, "You are registered successfully", $success);
    }

    public function login(){ 

        if(Auth::attempt(['email' => request('email'), 'password' => request('password'), 'user_type' => request('user_type'), 'is_active' => 1]))
        { 
            $user = Auth::user();

            $success['token'] =  $user->createToken('appName')->accessToken;
            $success['image_url'] =  url('storage/app').'/';
            User::where("id", "=", $user->id)->update(["api_token" => $success['token']]);
            // return response()->json(['success' => $success], $this->successStatus); 
            return $this->setResponseFormat($this->successStatus, "You are loggedin successfully", $success);
        } else{ 
            return $this->setResponseFormat(401, "Unauthorised User", "", 0);
            // return response()->json(['error'=>'Unauthorised'], 401);
        } 
    }

    public function getLoggedinUser() {
        
        $user = Auth::user();
        return $this->setResponseFormat($this->successStatus, "Logged In User Details", $user);
    }

    public function forgot_password(Request $request) {
        
        $email = $request->email;

        $email_exists = User::where('email', '=', $email)->count();
        $user_dtl = User::select(['name', 'email'])->where('email', '=', $email)->first();
        
        if($email != "" && $email_exists > 0)
        {
            $new_password = mt_rand(11111111, 99999999);

            $pass_upd = User::where('email', '=', $email)->update(['password' => bcrypt($new_password)]);

            if($pass_upd) // MailForgotPassword
            {
                $usr_dtl['password'] = $new_password;
                $usr_dtl['email'] = $email;
                $usr_dtl['name'] = $user_dtl->email;
                // echo $user_dtl->password; exit;
                // Mail::to($email)->send(new MailForgotPassword($user_dtl));

                Mail::send('mails.forgot-password', $usr_dtl, function($message) use ($usr_dtl){
 
                    $message->to($usr_dtl['email'], ucwords($usr_dtl['name']))
         
                            ->subject('Recover Forgot Password');
                });

                if(count(Mail::failures()) > 0)
                {
                    return $this->setResponseFormat(400, "Failed to send password on your email, Please try again.", Mail::failures(), 0);
                }
                else
                {
                    return $this->setResponseFormat($this->successStatus, "Password has been sent successfully to your email ".$email, "");
                }
            }
        }
        else
        {
            if($email == "")
            {
                return $this->setResponseFormat(400, "Please enter the email first.", "", 0);
            }
            else
            {
                return $this->setResponseFormat(400, "Your are not registered with us. Please register first", "", 0);
            }
        }
    }
} 