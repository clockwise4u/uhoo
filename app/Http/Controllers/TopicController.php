<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Interest;
use App\Models\Topic;
use App\Models\Image;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class TopicController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = DB::table('tbl_topics as tt')
        // ->join('tbl_images as ti', "tt.id", "=", "ti.type_id")
        ->join('tbl_images as ti', function($join)
        {
            $join->on("tt.id", "=", "ti.type_id")
                ->where("ti.type", "=", 1)
                ->whereNull('ti.deleted_at');
        })
        ->select([
            'tt.unique_id',
            'tt.topic_name',
            'tt.is_active',
            'ti.filename',
            'tt.is_active',
        ])
        ->whereNull("tt.deleted_at")
        ->whereNull("ti.deleted_at")
        ->get()->toArray();

        // echo "<pre>"; print_r($topics); exit;

        $data = array(
            'title'         => 'Topics List',
            'active_tab'    => 'topics',
            'topics'        => $topics,
        );
        return view('topics.index')->with($data);
    }

    /**
     * Get the Company list.
     * 
     */
    public function list()
    {
        $topics = DB::table('tbl_topics')
        ->select([
            'unique_id',
            'topic_name',
            'is_active',
        ])
        ->whereNull("deleted_at");

        $start = (\Request::get('start') == 0) ? 1 : \Request::get('start') + 1;

        return Datatables::of($topics)->rawColumns(['is_active', 'action'])
            ->addColumn('s_no', function($topics) use (&$start) {
               return $start++;
            })
            ->addColumn('topics_name', function ($topics) {

                return ucwords($topics->topics_name);
            })
            ->addColumn('is_active', function ($topics) {

                $active_check = '<label class="check_box"><input type="checkbox" class="change-status" data-uid="'. $topics->unique_id. '" '. ($topics->is_active == "1" ? "checked" : "") .'><span class="checkmark"></span></label>';

                return $active_check;
            })
            ->addColumn('action', function ($topics) {

                $action = '<a href="javascript:void(0);" class="edit-topics" data-uid="'. $topics->unique_id .'" data-val="'. $topics->topics_name .'">Edit</a> | <a href="javascript:void(0);" class="delete-topics" data-uid="'. $topics->unique_id .'">Delete</a>';

                return $action;
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        $data = array(
            'title'         => 'Add New Gata Sankhya',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        return view('gatasankhya.create')->with($data);
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic_details['topic_name'] = $request->topic_name;
        $topic_details['unique_id'] = md5(time().$request->_token);

        $topic_detail = Topic::create($topic_details);

        if(@$topic_detail->id)
        {

            $encoded_image = $request->image_data;  // your base64 encoded

            $image_array_1  = explode(";", $encoded_image);
            $image_array_1a = explode("/", $image_array_1[0]);
            $image_array_2  = explode(",", $image_array_1[1]);
            
            $dir_path = storage_path(). '/app/topic_images/';

            if(!\File::exists($dir_path))
            {
                \File::makeDirectory($dir_path, $mode = 0777, true, true);
            }

            $imageName = str_random(16).'.'.$image_array_1a[1];

            \File::put(storage_path(). '/app/topic_images/' . $imageName, base64_decode($image_array_2[1]));

            @chmod('storage/app/topic_images', 0777);
            @chmod('storage/app/topic_images/'.$imageName, 0777);

            $topic_image = 'topic_images/' . $imageName;

            $topic_image_data = array(

                'type' => 1,
                'type_id' => $topic_detail->id,
                'filename' => $topic_image,
            );

            $added_img = Image::create($topic_image_data);

            if(@$added_img->id)
            {
                echo "1"; exit;
            }
            else
            {
                echo "2"; exit;
            }

        }
        else
        {
            echo "2"; exit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($uid)
    {
        $data = array(
            'title'         => 'Gata Sankhya List',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        
        if($uid != "")
        {
            $gatasankhya_detail = GataSankhya::with("project", "company")->where("unique_id", "=", $uid)->first();
            // echo "<pre>"; print_r($gatasankhya_detail); exit;
            return view("gatasankhya.edit", compact("gatasankhya_detail"))->with($data);
        }
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $http_request(method, url)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid)
    {
        $interest_details['topics_name'] = $request->topics_name;
        // $interest_details['unique_id'] = md5(time().$request->_token);

        $interest_upd = Interest::where("unique_id", "=", $uid)->update($interest_details);

        if($interest_upd)
        {
            echo "1"; exit;
            // return redirect('interest')->with('success', 'Interest created Successfully');
        }
        else
        {
            echo "2"; exit;
            // return redirect('interest')->with('error', 'Unable to create topics. Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid)
    {
        if($uid != "")
        {
            $del_intrst = Interest::where("unique_id", "=", $uid)->delete();

            if($del_intrst)
            {
                echo "1"; exit;
            }
            else
                echo "2"; exit;
        }
        else
        {
            echo "2"; exit;
        }
    }

    // get inerest by ID
    public function get_inerest_by_uid($uid)
    {
        if($uid != "")
        {
            return Interest::where("unique_id", "=", $uid)->first();
        }
        else
        {
            return false;
        }
    }
}
