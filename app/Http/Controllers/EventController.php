<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interest;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class EventController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'title'         => 'Interest List',
            'active_tab'    => 'interest',
        );
        return view('interest.index')->with($data);
    }

    /**
     * Get the Company list.
     * 
     */
    public function list()
    {
        $interest = DB::table('tbl_interests')
        ->select([
            'unique_id',
            'interest_name',
            'is_active',
        ])
        ->whereNull("deleted_at");

        $start = (\Request::get('start') == 0) ? 1 : \Request::get('start') + 1;

        return Datatables::of($interest)->rawColumns(['is_active', 'action'])
            ->addColumn('s_no', function($interest) use (&$start) {
               return $start++;
            })
            ->addColumn('interest_name', function ($interest) {

                return ucwords($interest->interest_name);
            })
            ->addColumn('is_active', function ($interest) {

                $active_check = '<label class="check_box"><input type="checkbox" class="change-status" data-uid="'. $interest->unique_id. '" '. ($interest->is_active == "1" ? "checked" : "") .'><span class="checkmark"></span></label>';

                return $active_check;
            })
            ->addColumn('action', function ($interest) {

                $action = '<a href="javascript:void(0);" class="edit-interest" data-uid="'. $interest->unique_id .'" data-val="'. $interest->interest_name .'">Edit</a> | <a href="javascript:void(0);" class="delete-interest" data-uid="'. $interest->unique_id .'">Delete</a>';

                return $action;
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        $data = array(
            'title'         => 'Add New Gata Sankhya',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        return view('gatasankhya.create')->with($data);
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interest_details['interest_name'] = $request->interest_name;
        $interest_details['unique_id'] = md5(time().$request->_token);

        $interest_detail = Interest::create($interest_details);

        if(@$interest_detail->id)
        {
            echo "1"; exit;
            // return redirect('interest')->with('success', 'Interest created Successfully');
        }
        else
        {
            echo "2"; exit;
            // return redirect('interest')->with('error', 'Unable to create interest. Please try again.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($uid)
    {
        $data = array(
            'title'         => 'Gata Sankhya List',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        
        if($uid != "")
        {
            $gatasankhya_detail = GataSankhya::with("project", "company")->where("unique_id", "=", $uid)->first();
            // echo "<pre>"; print_r($gatasankhya_detail); exit;
            return view("gatasankhya.edit", compact("gatasankhya_detail"))->with($data);
        }
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $http_request(method, url)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid)
    {
        $interest_details['interest_name'] = $request->interest_name;
        // $interest_details['unique_id'] = md5(time().$request->_token);

        $interest_upd = Interest::where("unique_id", "=", $uid)->update($interest_details);

        if($interest_upd)
        {
            echo "1"; exit;
            // return redirect('interest')->with('success', 'Interest created Successfully');
        }
        else
        {
            echo "2"; exit;
            // return redirect('interest')->with('error', 'Unable to create interest. Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid)
    {
        if($uid != "")
        {
            $del_intrst = Interest::where("unique_id", "=", $uid)->delete();

            if($del_intrst)
            {
                echo "1"; exit;
            }
            else
                echo "2"; exit;
        }
        else
        {
            echo "2"; exit;
        }
    }

    // get inerest by ID
    public function get_inerest_by_uid($uid)
    {
        if($uid != "")
        {
            return Interest::where("unique_id", "=", $uid)->first();
        }
        else
        {
            return false;
        }
    }
}
