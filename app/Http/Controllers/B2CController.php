<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interest;
use App\Models\Customer;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class B2CController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'title'         => 'Customer List',
            'active_tab'    => 'b2c',
        );
        return view('b2c.index')->with($data);
    }

    /**
     * Get the Company list.
     * 
     */
    public function list()
    {
        $customers = DB::table('users as tu')
            ->join('tbl_customers as tc', "tu.id", "=", "tc.user_id")
            ->leftJoin('tbl_locations as tl', "tc.location_id", "=", "tl.id")
            ->leftJoin('tbl_images as ti', function($leftJoin)
            {
                $leftJoin->on("tu.id", "=", "ti.type_id")
                    ->where("ti.type", "=", 4)
                    ->whereNull('ti.deleted_at');
            })
            ->select([
                'tu.*',
                // 'tc.dob',
                // 'tc.title',
                // 'tc.description',
                // 'tc.interests',
                // 'tc.location_id',
                'tl.city',
                'tl.state',
                'tu.is_active',
                'ti.filename',
            ])
            ->where("tu.user_type", "=", '1')
            ->whereNull("tu.deleted_at")
            ->whereNull("tc.deleted_at");
            // ->get()->toArray();

        $start = (\Request::get('start') == 0) ? 1 : \Request::get('start') + 1;

        return Datatables::of($customers)->rawColumns(['action'])
            ->addColumn('s_no', function($customers) use (&$start) {
               return $start++;
            })
            ->addColumn('is_active', function ($customers) {

                // return ucwords($customers->interest_name);
                return $customers->is_active == "1" ? "Yes" : "No";
            })
            ->addColumn('location', function ($customers) {

                $city = $customers->city != "" ? $customers->city : "";
                $state = $customers->state != "" ? $customers->state : "";

                if($city != "" && $state != "")
                    $location = ucwords($city) .', '. ucwords($state);
                if($city != "" && $state == "")
                    $location = ucwords($city);
                if($city == "" && $state != "")
                    $location = ucwords($state);
                if($city == "" && $state == "")
                    $location = "N/A";

                return $location;
            })
            ->addColumn('action', function ($customers) {

                $action = '<a href="'. url('b2c') .'/'. $customers->unique_id .'" class="btn btn_view a-btn-slide-text1"><span><strong>View</strong></span></a>';

                return $action;
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        $data = array(
            'title'         => 'Add New Gata Sankhya',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        return view('gatasankhya.create')->with($data);
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interest_details['interest_name'] = $request->interest_name;
        $interest_details['unique_id'] = md5(time().$request->_token);

        $interest_detail = Interest::create($interest_details);

        if(@$interest_detail->id)
        {
            echo "1"; exit;
            // return redirect('interest')->with('success', 'Interest created Successfully');
        }
        else
        {
            echo "2"; exit;
            // return redirect('interest')->with('error', 'Unable to create interest. Please try again.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($uid)
    {
        $data = array(
            'title'         => 'Gata Sankhya List',
            'main_tab'      => 'basic_setting',
            'active_tab'    => 'gata-sankhya',
        );
        
        if($uid != "")
        {
            $gatasankhya_detail = GataSankhya::with("project", "company")->where("unique_id", "=", $uid)->first();
            // echo "<pre>"; print_r($gatasankhya_detail); exit;
            return view("gatasankhya.edit", compact("gatasankhya_detail"))->with($data);
        }
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $http_request(method, url)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid)
    {
        $interest_details['interest_name'] = $request->interest_name;
        // $interest_details['unique_id'] = md5(time().$request->_token);

        $interest_upd = Interest::where("unique_id", "=", $uid)->update($interest_details);

        if($interest_upd)
        {
            echo "1"; exit;
            // return redirect('interest')->with('success', 'Interest created Successfully');
        }
        else
        {
            echo "2"; exit;
            // return redirect('interest')->with('error', 'Unable to create interest. Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid)
    {
        if($uid != "")
        {
            $del_intrst = Interest::where("unique_id", "=", $uid)->delete();

            if($del_intrst)
            {
                echo "1"; exit;
            }
            else
                echo "2"; exit;
        }
        else
        {
            echo "2"; exit;
        }
    }

    // get inerest by ID
    public function get_inerest_by_uid($uid)
    {
        if($uid != "")
        {
            return Interest::where("unique_id", "=", $uid)->first();
        }
        else
        {
            return false;
        }
    }
}
