         
         <aside class="col-sm-3 sidebar">
            <div class="logo"><img src="{{ url('assets/images/logo.png') }}"></div>
            <div class="navigation">
               <ul>
                  <li>
                     <a class="active" href="{{ url('/') }}">
                     <span><img src="{{ url('assets/images/gridicon.png') }}"></span>
                     Dashboard
                     </a>
                  </li>
                  <li class="dropdown show">
                     <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="true">
                     <span><img src="{{ url('assets/images/user1.png') }}"></span>
                     Customers
                     </a>
                     <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -242px, 0px);">
                        <li class="">
                           <a href="{{ url('b2b')
                            }}">B2B</a>
                        </li>
                        <li class="">
                           <a href="{{ url('b2c') }}">B2C</a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="{{ url('interest') }}">
                     <span><img src="{{ url('assets/images/interest.png') }}"></span>
                     Interest
                     </a>
                  </li>
                  <li>
                     <a href="{{ url('topics') }}">
                     <span><img src="{{ url('assets/images/topic_icon.png') }}"></span>
                     Topic
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);{{-- url('offers') --}}">
                     <span><img src="{{ url('assets/images/offer.png') }}"></span>
                     Offers
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);{{-- url('events') --}}">
                     <span><img src="{{ url('assets/images/events.png') }}"></span>
                     Events
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);">
                     <span><img src="{{ url('assets/images/rewards.png') }}"></span>
                     Rewards
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);">
                     <span><img src="{{ url('assets/images/survey.png') }}"></span>
                     Survey
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);">
                     <span><img src="{{ url('assets/images/reports.png') }}"></span>
                     Report
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0);{{-- url('faqs') --}}">
                     <span><img src="{{ url('assets/images/faq.png') }}"></span>
                     FAQ
                     </a>
                  </li>
                  <li>
                     <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span><img src="{{ url('assets/images/logout.png') }}"></span>
                     Logout
                     </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  </li>
               </ul>
            </div>
         </aside>