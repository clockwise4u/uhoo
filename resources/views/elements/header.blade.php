
            <header class="top_header">
               <div class="header-area">
                  <div class="row align-items-center">
                     <!-- nav and search button -->
                     <div class="col-md-6 hder_serch_bar1">
                        <div class="nav-btn pull-left">
                           <button type="button" class="btn btn-default"> 
                           <span></span>
                           <span></span>
                           <span></span></button>
                        </div>
                     </div>
                     <div class="col-md-6 hder_serch_bar">
                        <ul class="navbar2">
                           <li class="nav-item2">
                              <a class="nav-link">Welcome, {{ ucwords(\Auth::user()->name) }}</a>
                           </li>
                           <li class="nav-item2">
                              <a class="nav-link" href="#"><img src="{{ url('assets/images/placeholder.png') }}"></a>
                           </li>
                           <li class="nav-item2">
                              <a class="nav-link" href="#"></a>
                              <div class="dropdown">
                                 <i class="fa fa-caret-down" aria-hidden="true"></i>
                                 <div class="dropdown-content">
                                    <a href="#">Profile</a>
                                    <a href="#">Setting</a>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

{{--                                     <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a> --}}

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </header>