@extends('layouts.master')

@section('custom_css')

@endsection

@section('content')

<section class="listed_property">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-12">
            <div class="dt-card">
                <div class="dt-card__body px-5 py-4">
                    <h6 class="text-body text-uppercase mb-2">Total Business  Joined</h6>
                    <div class="d-flex align-items-baseline mb-4">
                        <span class="display-4 font-weight-500 text-dark mr-2">26,873</span>
                        <div class="d-flex align-items-center">
                            <span class="f-12"><span class="value">7</span> New Business added this week</span>
                        </div>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-12">
            <div class="dt-card">
                <div class="dt-card__body px-5 py-4">
                    <h6 class="text-body text-uppercase mb-2">Total Events</h6>
                    <div class="d-flex align-items-baseline mb-4">
                        <span class="display-4 font-weight-500 text-dark mr-2">384</span>
                        <div class="d-flex align-items-center">
                            <span class="f-12"><span class="value">7</span> New events this week</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-12">
            <div class="dt-card">
                <div class="dt-card__body px-5 py-4">
                    <h6 class="text-body text-uppercase mb-2">Total Offers</h6>
                    <div class="d-flex align-items-baseline mb-4">
                        <span class="display-4 font-weight-500 text-dark mr-2">84,729</span>
                        <div class="d-flex align-items-center">
                            <span class="f-12"><span class="value">7</span> New offers this week</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="team" class="pb-5 business_section">
    <div class="col-12 headings_inr view_all">
        <h1>Recently Added Businesses</h1>
        <span><a href="b2b.html" class="btn btn_view a-btn-slide-text1">
            <span><strong>View All</strong></span>
        </a></span>
    </div>
    <div class="col-12 line_img">
        <span><img src="{{ url('assets/images/line.png') }}"></span>
    </div>
    <div class="row owl-carousel">
        <div class="col-lg-3 col-md-6">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <a class="card_link" href="b2b-detail.html"><div class="card-body text-center">
                                    <p><img class=" img-fluid" src="{{ url('assets/images/team1.png') }}" alt="card image"></p>
                                    <h4 class="card-title">KidZania Delhi NCR</h4>
                                    <p class="card-text">Entry Tickets for Kids with Adults</p>
                                    <p class="locations">
                                        <span class="locate">Location</span>
                                        <span class="price">San Francisco, CA</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="backside">
                        <div class="card">
                            <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                                <h4 class="card-title">KidZania Delhi NCR</h4>
                                <p class="card-text">Entry Tickets for Kids with Adults</p>
                                <p class="locations">
                                    <span class="locate">Location</span>
                                    <span class="price">San Francisco, CA</span>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"><div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/team2.png') }}" alt="card image"></p>
                            <h4 class="card-title">Oysters Water Park</h4>
                            <p class="card-text">Entry Tickets to Water Park</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Kansas City, KS</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                        <h4 class="card-title">Oysters Water Park</h4>
                        <p class="card-text">Entry Tickets to Water Park</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">Kansas City, KS</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"><div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/team3.png') }}" alt="c.0.0ard image"></p>
                            <h4 class="card-title">Worlds of Wonder</h4>
                            <p class="card-text">Entry Tickets to Water & Amusement Park</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Seattle, WA</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                        <h4 class="card-title">Worlds of Wonder</h4>
                        <p class="card-text">Entry Tickets to Water & Amusement Park</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">Seattle, WA</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"><div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/team4.png') }}" alt="card image"></p>
                            <h4 class="card-title">Madame Tussauds Delhi</h4>
                            <p class="card-text">35% off on Entry Tickets</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Reading, PA</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                        <h4 class="card-title">Madame Tussauds Delhi</h4>
                        <p class="card-text">35% off on Entry Tickets</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">Reading, PA</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="row owl-carousel carsel_second">
    <div class="col-lg-3 col-md-6">
        <div class="item">
            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                <div class="mainflip">
                    <div class="frontside">
                        <div class="card">
                            <a class="card_link" href="b2b-detail.html"><div class="card-body text-center">
                                <p><img class=" img-fluid" src="{{ url('assets/images/recent_ofr1.png') }}" alt="card image"></p>
                                <h4 class="card-title">KidZania Delhi NCR</h4>
                                <p class="card-text">Entry Tickets for Kids with Adults</p>
                                <p class="locations">
                                    <span class="locate">Location</span>
                                    <span class="price">Santa Ana, CA</span>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="backside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                            <h4 class="card-title">KidZania Delhi NCR</h4>
                            <p class="card-text">Entry Tickets for Kids with Adults</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Santa Ana, CA</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"> <div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/recent_ofr2.png') }}" alt="card image"></p>
                            <h4 class="card-title">Oysters Water Park</h4>
                            <p class="card-text">Entry Tickets to Water Park</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">NYC, NY</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                        <h4 class="card-title">Oysters Water Park</h4>
                        <p class="card-text">Entry Tickets to Water Park</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">NYC, NY</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"> <div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/recent_ofr3.png') }}" alt="c.0.0ard image"></p>
                            <h4 class="card-title">Worlds of Wonder</h4>
                            <p class="card-text">Entry Tickets to Water & Amusement Park</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Venice Beach, CA</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"><div class="card-body text-center mt-4">
                        <h4 class="card-title">Worlds of Wonder</h4>
                        <p class="card-text">Entry Tickets to Water & Amusement Park</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">Venice Beach, CA</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="item">
        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
                <div class="frontside">
                    <div class="card">
                        <a class="card_link" href="b2b-detail.html"> <div class="card-body text-center">
                            <p><img class=" img-fluid" src="{{ url('assets/images/recent_ofr4.png') }}" alt="card image"></p>
                            <h4 class="card-title">Madame Tussauds Delhi</h4>
                            <p class="card-text">35% off on Entry Tickets</p>
                            <p class="locations">
                                <span class="locate">Location</span>
                                <span class="price">Seattle, WA</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="backside">
                <div class="card">
                    <a class="card_link" href="b2b-detail.html"> <div class="card-body text-center mt-4">
                        <h4 class="card-title">Madame Tussauds Delhi</h4>
                        <p class="card-text">35% off on Entry Tickets</p>
                        <p class="locations">
                            <span class="locate">Location</span>
                            <span class="price">Seattle, WA</span>
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section>

<section id="offers" class="pb-5">
    <div class="col-12 headings_inr view_all">
        <h1>Latest Offers</h1>
        <span><a href="offers.html" class="btn btn_view a-btn-slide-text1">
            <span><strong>View All</strong></span>
        </a></span>
    </div>
    <div class="col-12 line_img">
        <span><img src="{{ url('assets/images/line.png') }}"></span>
    </div>
    <div class="row owl-carousel slider2">
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/offer1.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">PVR Contest</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 1 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">Nashville, TN</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">PVR Contest</h4>
                                        <p class="card-text">Free Movie Tickets for 1 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">Nashville, TN</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/offer2.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">Win of The Week</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 2 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">Ventura, CA</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">Win of The Week</h4>
                                        <p class="card-text">Free Movie Tickets for 2 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">Ventura, CA</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/offer3.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">Win of The Week</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 2 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">NYC, NY</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">Win of The Week</h4>
                                        <p class="card-text">Free Movie Tickets for 2 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">NYC, NY</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row owl-carousel slider2">
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/Contest1.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">PVR Contest</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 1 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">San Juan Capistrano, CA</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">PVR Contest</h4>
                                        <p class="card-text">Free Movie Tickets for 1 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">San Juan Capistrano, CA</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/Contest2.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">Win of The Week</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 2 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">Seattle, WA</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">Win of The Week</h4>
                                        <p class="card-text">Free Movie Tickets for 2 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">Seattle, WA</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card offer_iners">
                                <div class="card-body text-center">
                                    <div class="display_flex">
                                        <div class="offer_img">
                                            <p><img class=" img-fluid" src="{{ url('assets/images/Contest3.png') }}" alt="card image"></p>
                                        </div>
                                        <div class="offer_content">
                                            <h4 class="card-title win">Win of The Week</h4>
                                            <p class="card-text tickets">Free Movie Tickets for 2 Month</p>
                                            <div class="left-img2">
                                                <span class="special">SPECIAL20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="locations nashville location_nashville">
                                        <span class="locate">Location</span>
                                        <span class="price">NYC, NY</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <div class="offer_content1">
                                        <h4 class="card-title win">Win of The Week</h4>
                                        <p class="card-text">Free Movie Tickets for 2 Month</p>
                                        <p class="locations nashville">
                                            <span class="locate">Location</span>
                                            <span class="price">NYC, NY</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="events" class="pb-5">
    <div class="col-12 headings_inr view_all">
        <h1>Latest Events</h1>
        <span><a href="events.html" class="btn btn_view a-btn-slide-text1">
            <span><strong>View All</strong></span>
        </a></span>
    </div>
    <div class="col-12 line_img">
        <span><img src="{{ url('assets/images/line.png') }}"></span>
    </div>
    <div class="row owl-carousel">
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event1.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Buster</h4>
                                        <div class="denmark">
                                            <p>From Denmark</p>
                                            <p>June 13, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img1.png') }}"><span>BY <b>GRANYAN</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Buster</h4>
                                    <div class="denmark">
                                        <p>From Denmark</p>
                                        <p>June 13, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img1.png') }}"><span>BY <b>GRANYAN</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event2.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Max Korzh - Official website</h4>
                                        <div class="denmark">
                                            <p>From Ukraine</p>
                                            <p>June 09, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Max Korzh - Official website</h4>
                                    <div class="denmark">
                                        <p>From Ukraine</p>
                                        <p>June 09, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event3.png') }}" alt="card image"></p>
                                        <h4 class="card-title">SOL Beachclub</h4>
                                        <div class="denmark">
                                            <p>From Austria</p>
                                            <p>June 03, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY<b>NC-WERBUNG</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Max Korzh - Official website</h4>
                                    <div class="denmark">
                                        <p>From Ukraine</p>
                                        <p>June 09, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event4.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Schloß Hollenegg for Design</h4>
                                        <div class="denmark">
                                            <p>From Spain</p>
                                            <p>May 31, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img4.png') }}"><span>BY <b>HINT</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Schloß Hollenegg for Design</h4>
                                    <div class="denmark">
                                        <p>From Spain</p>
                                        <p>May 31, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img4.png') }}"><span>BY <b>HINT</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row owl-carousel">
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event5.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Buster</h4>
                                        <div class="denmark">
                                            <p>From Denmark</p>
                                            <p>June 13, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img1.png') }}"><span>BY <b>GRANYAN</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Buster</h4>
                                    <div class="denmark">
                                        <p>From Denmark</p>
                                        <p>June 13, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img1.png') }}"><span>BY <b>GRANYAN</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event6.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Max Korzh - Official website</h4>
                                        <div class="denmark">
                                            <p>From Ukraine</p>
                                            <p>June 09, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Max Korzh - Official website</h4>
                                    <div class="denmark">
                                        <p>From Ukraine</p>
                                        <p>June 09, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event7.png') }}" alt="card image"></p>
                                        <h4 class="card-title">SOL Beachclub</h4>
                                        <div class="denmark">
                                            <p>From Austria</p>
                                            <p>June 03, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY<b>NC-WERBUNG</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Max Korzh - Official website</h4>
                                    <div class="denmark">
                                        <p>From Ukraine</p>
                                        <p>June 09, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img2.png') }}"><span>BY <b>ADVANCED.TEAM</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="item">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <a href="http://creativebuffer.com/uhoo-app/admin/event_detail.html">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ url('assets/images/event8.png') }}" alt="card image"></p>
                                        <h4 class="card-title">Schloß Hollenegg for Design</h4>
                                        <div class="denmark">
                                            <p>From Spain</p>
                                            <p>May 31, 2019</p>
                                        </div>
                                        <p class="logo_img"><img src="{{ url('assets/images/logo_img4.png') }}"><span>BY <b>HINT</b></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">Schloß Hollenegg for Design</h4>
                                    <div class="denmark">
                                        <p>From Spain</p>
                                        <p>May 31, 2019</p>
                                    </div>
                                    <p class="logo_img"><img src="{{ url('assets/images/logo_img4.png') }}"><span>BY <b>HINT</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('custom_js')

    <script type="text/javascript">
        
        $(document).ready(function() {
            $('.myTable').DataTable({
                  "scrollX": true
            });
        });

    </script>
@endsection

