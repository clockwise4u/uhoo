@extends('layouts.master')

@section('custom_css')
<style type="text/css">
    .table
    {
        width: 100% !important;
        min-width: 0px;
    }

    #myDiv
    {
        text-align: left;
    }

    .form-error
    {
        font-size: 14px;
        padding-top: 6px;
        margin: 0px;
        font-weight: normal !important;
        color: #ff0000 !important;
    }
</style>

<!-- Sweet Alert -->
<link rel="stylesheet" href="{{ url('assets/css/sweetalert.css')  }}">

@endsection

@section('content')            

<div class="row full">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="breadcrumb_sec">
            <ul class="breadcrumb topic">
                <li class="active msg"><a class="inactive_bread" href="interest.html">Interest</a> </li>
            </ul>
        </div>
        <div class="patient_back_section single_patinent_bg apoinmnts">
            <div class="future-apointmnt bottom_top Medication">
                <div class="patient_insight data_pading"></div>
                <button type="button" class="btn btn-primary book_now" data-toggle="modal" data-target="#myModal">Add New </button>
            </div>
            <div class="background_sec padding_inside">
                <table class="interest-list display joined_data table table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Interest Name</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Interest</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="my-model" id="add-interest">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="interest_name">Interest Name</label>
                            <input type="text" class="form-control" placeholder="Enter Interest Name" id="interest_name" name="interest_name">
                            <label class="form-error error-interest_name" style="display: none;">Please enter Inerest Name</label>
                        </div>
                    </div>
                    <div class="form-group btnGrp col-md-12">
                        <button type="button" class="btn btn-primary add-interest-btn">Save</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Interest</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="my-model" id="edit-interest">
                    <div class="form-row">
                        <input type="hidden" name="unique_id" id="unique_id">
                        <div class="form-group col-md-12">
                            <label for="interest_name-edit">Interest Name</label>
                            <input type="text" class="form-control" placeholder="Enter Interest Name" id="interest_name" name="interest_name">
                            <label class="form-error error-interest_name-edit" style="display: none;">Please enter Interest Name</label>
                        </div>
                    </div>
                    <div class="form-group btnGrp col-md-12">
                        <button type="button" class="btn btn-primary edit-interest-btn">Update</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

    {{-- Sweet Alert --}}
    <script src="{{ url('assets/js/sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        
        $(document).ready(function() {
            
            oTable = $('.interest-list').DataTable({

                // dom: 'Blfrtip',
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                responsive: true,
                processing: true,
                serverSide: true,
                filter:true,
                ajax: {
                    url: '{{ url('interest/list') }}',
                    /*data: function(d){
                        d.company_id = $('#company_id').val();
                        d.project_id = $('#project_id').val();
                        d.search = {value:$('#search').val()}
                    }*/
                },
                columns: [
                    { data: 's_no', name: 's_no', sortable: false },
                    { data: 'interest_name', name: 'interest_name', sortable: false },
                    { data: 'is_active', name: 'is_active', sortable: false },
                    { data: 'action', name: 'action', sortable: false }
                ]
            });

            $('#search').keyup(function(){
                oTable.draw();
            });

            $(".add-interest-btn").click(function(){

                // alert("hello");
                if($("#add-interest #interest_name").val() != "")
                {
                    $(".error-interest_name").hide();
                    var interest_name = $("#add-interest #interest_name").val();

                    $.ajax({
                        url: "{{ url('interest') }}",
                        type: "POST",
                        data: {
                            interest_name: interest_name,
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(resp){
                            if(resp == "1")
                            {
                                swal({
                                    title: "Added!",
                                    text: "Interest Added Successfully.",
                                    type: "success",
                                    showConfirmButton: true,
                                    // timer: 1000,
                                },
                                function () {

                                    $("#myModal").modal("hide");
                                    oTable.draw();
                                });
                            }
                            else
                            {
                                swal({
                                    title: "Sorry!",
                                    text: "Unable to add this inerest. Please try again later.",
                                    type: "error",
                                    closeOnConfirm: false,
                                    timer: 1000,
                                }, 
                                function () {

                                    $("#myModal").modal("hide");
                                    // oTable.draw();
                                });
                            }
                        }
                    });
                }
                else
                {
                    $(".error-interest_name").show();
                }
            });

            $(document).on("click", ".edit-interest", function(){

                // alert("hello");
                var uid = $(this).data("uid");
                var interest = $(this).data("val");

                $("#edit-interest #interest_name").val(interest);
                $("#edit-interest #unique_id").val(uid);
                $(".error-interest_name").hide();
                $("#myModal-edit").modal("show");
            });

            $(".edit-interest-btn").click(function(){

                // alert("hello");
                if($("#edit-interest #interest_name").val() != "")
                {
                    $(".error-interest_name").hide();
                    var interest_name = $("#edit-interest #interest_name").val();
                    var unique_id = $("#edit-interest #unique_id").val();

                    $.ajax({
                        url: "{{ url('interest') }}/"+unique_id,
                        type: "POST",
                        data: {
                            interest_name   : interest_name,
                            _method: "PATCH"
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(resp){
                            if(resp == "1")
                            {
                                swal({
                                    title: "Updated!",
                                    text: "Interest Updated Successfully.",
                                    type: "success",
                                    showConfirmButton: true,
                                    // timer: 1000,
                                },
                                function () {

                                    $("#myModal-edit").modal("hide");
                                    oTable.draw();
                                });
                            }
                            else
                            {
                                swal({
                                    title: "Sorry!",
                                    text: "Unable to add this inerest. Please try again later.",
                                    type: "error",
                                    closeOnConfirm: false,
                                    timer: 1000,
                                }, 
                                function () {

                                    $("#myModal-edit").modal("hide");
                                    // oTable.draw();
                                });
                            }
                        }
                    });
                }
                else
                {
                    $(".error-interest_name").show();
                }
            });

            $(document).on("click", ".delete-interest", function(){

                var uid = $(this).data("uid");

                if(uid != ""){

                    swal({
                        title: "Are you sure?",
                        text: "You really want to delete this Interest..!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function(){

                            $.ajax({
                                url: "{{ url('interest') }}/"+uid,
                                type: "POST",
                                data: {
                                    _method: "DELETE"
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(resp){
                                    if(resp == "1")
                                    {
                                        swal({
                                            title: "Deleted!",
                                            text: "Interest Deleted Successfully.",
                                            type: "success",
                                            showConfirmButton: true,
                                            // timer: 1000,
                                        },
                                        function () {

                                            // $("#myModal-edit").modal("hide");
                                            oTable.draw();
                                        });
                                    }
                                    else
                                    {
                                        swal({
                                            title: "Sorry!",
                                            text: "Unable to add this inerest. Please try again later.",
                                            type: "error",
                                            closeOnConfirm: true,
                                            // timer: 1000,
                                        }, 
                                        function () {

                                            $("#myModal-edit").modal("hide");
                                            // oTable.draw();
                                        });
                                    }
                                }
                            });

                    });
                }
                else
                {
                    // $(".error-interest_name").show();
                    alert("interest not found")
                }
            });
        });

    </script>
    
@endsection