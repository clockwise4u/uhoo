<!DOCTYPE html>
<html lang="en-US">
   <head>
      <title>Dashboard</title>
      <meta name="keyword" content="" />
      <meta name="description" content="" />
      <meta charset="utf-8" />
      <meta http-equiv="x-ua-compatible" content="ie=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <link rel="shortcut icon" type="image/png" href="{{ ('assets/images/icon/favicon.ico') }}" />
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
      <link href="{{ ('assets/css/bootstrap.css') }}" rel="stylesheet" />
      <link href="{{ ('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" />
      <link href="{{ ('assets/css/style.css') }}" rel="stylesheet" />
      {{-- <link rel="stylesheet" type="text/css" href="{{ ('assets/css/owl.carousel.min.css') }}" /> --}}
      
      @yield('custom_css')

      <style type="text/css">
        .modal
        {
          z-index: 1000000;
        }

        .sweet-alert
        {
          z-index: 1000002 !important;
        }

        .sweet-overlay
        {
          z-index: 1000001 !important;
        }
      </style>

   </head>
   <body onload="myFunction()">

   <div class="ring" id="loader">
      <span class="loader_inr"></span>
   </div>

    
    <div style="display:none;" id="myDiv" class="animate-bottom">
      <div class="main-sec">

         @include('elements/left-sidebar')

         <div class="col-sm-9 solitude">

         @include('elements/header')

            @yield('content')

         </div>

         <div class="overflow">
            <div class="cross_icon"><i class="fa fa-remove"></i></div>
         </div>

         </div>
      </div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script> -->
      <script src="{{ url('assets/js/popper.min.js') }}"></script>
      <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('assets/js/jquery.dataTables.min.js') }}"></script>

      <script>
         $(document).ready(function() {

            document.getElementsByTagName("html")[0].style.visibility = "visible";

            $(".nav-btn.pull-left").click(function(){
               $(".sidebar").toggleClass("active_sidebar");
               $(".main-sec").toggleClass("active_main");
               $(".solitude").toggleClass("active_solitude");
               $("body,html").toggleClass("active_body");
            });
            
            
            $(".overflow .cross_icon, .overflow").click(function(){
               $(".sidebar").removeClass("active_sidebar");
               $(".main-sec").removeClass("active_main");
               $(".solitude").removeClass("active_solitude");
               $("body,html").removeClass("active_body");
            });
         });

         function myFunction() {
           var copyText = document.getElementById("myInput");
           copyText.select();
           document.execCommand("copy");
           
           var tooltip = document.getElementById("myTooltip");
           tooltip.innerHTML = "Copied: " + copyText.value;
         }
         
         function outFunc() {
           var tooltip = document.getElementById("myTooltip");
           tooltip.innerHTML = "Copy to clipboard";
         }

         var myVar;

         function myFunction() {
           myVar = setTimeout(showPage, 1000);
         }

         function showPage() {
           document.getElementById("loader").style.display = "none";
           document.getElementById("myDiv").style.display = "block";
         }
      </script>

      <script src="{{ url('assets/js/owl.carousel.min.js') }}"></script>

      {{-- <script>
         $(".owl-carousel.slider2").owlCarousel({
             loop: true,
             autoplay:true,
             dots: false,
             nav: true,
             margin:10,
             responsive: {
                 320: {
                     items: 1
                 },
                 375: {
                     items: 1
                 },
                 425: {
                     items: 1
                 },
                 480: {
                     items: 1
                 },
                 600: {
                     items: 1
                 },
                 992: {
                     items: 2
                 }
             } 
         }); 
         
         $(".owl-carousel").owlCarousel({
             loop: true,
             autoplay:true,
             dots: false,
             nav: true,
             margin:10,
             responsive: {
                 320: {
                     items: 1
                 },
                 375: {
                     items: 1
                 },
                 425: {
                     items: 1
                 },
                 480: {
                     items: 1
                 },
                 600: {
                     items: 2
                 },
                 992: {
                     items: 4
                 }
             }
         }); 
      </script> --}}

      @yield('custom_js')
   </body>
</html>