@extends('layouts.master')

@section('custom_css')
<style type="text/css">
    .table
    {
        width: 100% !important;
        min-width: 0px;
    }

    #myDiv
    {
        text-align: left;
    }

    .form-error
    {
        font-size: 14px;
        padding-top: 6px;
        margin: 0px;
        font-weight: normal !important;
        color: #ff0000 !important;
    }

    body {
        background-color: #f5f5f5;
    }
    .imagePreview {
        width: 100%;
        height: 180px;
        background-position: center center;
        background: url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
        background-color: #fff;
        background-size: cover;
        background-repeat: no-repeat;
        display: inline-block;
        box-shadow: 0px -3px 6px 2px rgba(0, 0, 0, 0.2);
    }
    .btn-primary {
        display: block;
        border-radius: 0px;
        box-shadow: 0px 4px 6px 2px rgba(0, 0, 0, 0.2);
        margin-top: -5px;
    }
    .imgUp {
        margin-bottom: 15px;
    }
    .del {
        position: absolute;
        top: 0px;
        right: 15px;
        width: 30px;
        height: 30px;
        text-align: center;
        line-height: 30px;
        background-color: rgba(255, 255, 255, 0.6);
        cursor: pointer;
    }
    .imgAdd {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background-color: #4bd7ef;
        color: #fff;
        box-shadow: 0px 0px 2px 1px rgba(0, 0, 0, 0.2);
        text-align: center;
        line-height: 30px;
        margin-top: 0px;
        cursor: pointer;
        font-size: 15px;
    }
    .demo-bg{
        background: #ffac0c;
        margin-top: 60px;
    }
    .business-hours {
        background: #fff;
        padding: 0px 0px;
        margin-top: 0px;
        position: relative;
    }
    .business-hours:before{
        content: '';
        width: 23px;
        height: 23px;
        background: #111;
        position: absolute;
        top: 5px;
        left: -12px;
        transform: rotate(-45deg);
        z-index: -1;
    }
    .business-hours .title {
        text-transform: capitalize;
        padding-left: 0px;
        border-left: none;
        font-size: 15px;
        font-weight: 600;
        color: #333;
    }
    .business-hours li {
        color: #333;
        line-height: 30px;
        /*border-bottom: 1px solid #333;*/
        clear: both;
        padding: 10px 0;
    }
    .business-hours li:last-child {
        border-bottom: none; 
    }
    .business-hours .opening-hours li.today {
        color: #ffac0c; 
    }
    .business-hours input {
        border-bottom: 1px solid #222;
        -webkit-appearance: none;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: none;
    }
    textarea#exampleFormControlTextarea2 {
        min-height: 65px;
        margin-top: 0;
        border: 1px solid #ced4da;
        border-radius: 5px !important;
        padding: 5px 10px;
    }
    .tokenfield .token {
        height: auto !important;
        background-image: linear-gradient(to right, #934598, #884b9d, #6366b6, #4b71bd, #4072ba);
        color: #fff;
        font-size: 18px;
        margin: 4px 8px;
        padding: 5px 13px;
        float: left;
        height: auto !important;
    }

    .tokenfield .token .close {
        color: #fff;
        opacity: 1;
        font-size: 16px;
        margin: 0 3px;
    }
    .intrest.intrest2 .tokenfield.form-control {
        padding-bottom: 22px;
    }
</style>

<!-- Sweet Alert -->
<link rel="stylesheet" href="{{ url('assets/css/sweetalert.css')  }}">
<link rel="stylesheet" href="{{ url('assets/css/jquery.timepicker.min.css')  }}">

@endsection

@section('content') 

<div class="breadcrumb_sec">
    <ul class="breadcrumb">
        <li class="active msg"><a class="inactive_bread" href="b2b.html">B2B Customer List</a></li>
    </ul>
</div>
<section class="table_design">
    <div class="future-apointmnt bottom_top Medication">
        <div class="patient_insight data_pading"></div>
        <button type="button" class="btn btn-primary book_now" data-toggle="modal" data-target="#myModal">Add New Business</button>
    </div>
    <div class="padding_inside">
        <table class="myTable display joined_data">
            <thead>
                <tr>
                    <th>Business Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Heyinz</td>
                    <td>Timothy B Morgan</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Rumpes</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Jozzby</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Hariox</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Tiollo</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Mozzby</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Frizty</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Twiclo</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Clerby</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Zengvo</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Wrizty</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Zengary</td>
                    <td>2528 Chipmunk Lane</td>
                    <td>0987654321</td>
                    <td>abc@gmail.com</td>
                    <td>Active</td>
                    <td>
                        <a href="b2b-detail.html" class="btn btn_view a-btn-slide-text1">
                            <span><strong>Detail</strong></span>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>


<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New Business</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="my-model">
                    <div class="form-row">
                        <div class="form-group col-md-4 col-sm-3">
                            <p>Upload logo image</p>
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-8 col-sm-9">
                            <label for="inputSelectGp">Upload Slider image</label>
                            <div class="row">
                                <div class="col-md-4 col-sm-12 imgUp">
                                    <div class="imagePreview"></div>
                                    <label class="btn btn-primary">
                                        Upload
                                        <input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                    </label>
                                </div>
                                <!-- col-2 -->
                                <i class="fa fa-plus imgAdd"></i>
                            </div>
                            <!-- row -->
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSelectGp">Business Name</label>
                            <input type="text" class="form-control" placeholder="Business Name" id="inputCity">
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="inputSelectGp">Address</label>
                            <input type="text" class="form-control" placeholder="Address" id="inputCity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSelectGp">Phone</label>
                            <input type="text" class="form-control" placeholder="Phone" id="inputCity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSelectGp">Email</label>
                            <input type="text" class="form-control" placeholder="Email" id="inputCity">
                        </div>
                        <div class="form-group col-md-6">
                            <div class="stausflx">
                                <label for="inputSelectGp">Status</label>
                                <div class="active">
                                    <input type="radio" value="" name="a"> <span>Active</span>
                                </div>
                                <div class="inactive">
                                    <input type="radio" value="" name="a"> <span>Inactive</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSelectGp">Discription</label>
                            <textarea placeholder="Discription" class="form-control rounded-0 custom_textarea" id="exampleFormControlTextarea2" rows="1"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSelectGp">Tags</label>
                            <div class="intrest intrest2">
                                <input type="text" class="form-control" id="search" value="Arts,Film,Sports"  placeholder="" />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="business-hours">
                                <h2 class="title">Working Hours</h2>
                                <ul class="list-unstyled opening-hours">
                                    <li>Monday 
                                        <span class="pull-right">
                                            {{-- <input type="text" name="start_time[1]" class="time start" /> to  --}}
                                            {{-- <input type="text" name="end_time[1]" class="SZZtime end" /> --}}
                                            <input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM">
                                        </span>
                                    </li>
                                    <li>Tuesday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                    <li>Wednesday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                    <li>Thursday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                    <li>Friday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                    <li>Saturday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                    <li>Sunday <span class="pull-right"><input type="text" name="" value="" placeholder="11:00 AM to 11:00 PM"></span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group btnGrp col-md-6">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

{{-- Sweet Alert --}}
<script src="{{ url('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ url('assets/js/jquery.timepicker.min.js') }}"></script>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
                $('#image_data').val(e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });


    $(document).ready(function() {

        $('.myTable').DataTable({
            "scrollX": true,
            "paging": true,
            "ordering": false,
            "info": true
        });

        $('.time').timepicker({ 'timeFormat': 'h:i A'});
            
        /*oTable = $('.interest-list').DataTable({

            // dom: 'Blfrtip',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            processing: true,
            serverSide: true,
            filter:true,
            ajax: {
                url: '{{ url('interest/list') }}',
                // data: function(d){
                //     d.company_id = $('#company_id').val();
                //     d.project_id = $('#project_id').val();
                //     d.search = {value:$('#search').val()}
                // }
            },
            columns: [
                { data: 's_no', name: 's_no', sortable: false },
                { data: 'interest_name', name: 'interest_name', sortable: false },
                { data: 'is_active', name: 'is_active', sortable: false },
                { data: 'action', name: 'action', sortable: false }
            ]
        });

        $('#search').keyup(function(){
            oTable.draw();
        });*/

        $(".add-topic-btn").click(function(){

            alert("hello");
            if($("#add-topic #topic_name").val() != "" && $("#add-topic #image_data").val() != "")
            {
                $(".form-error").hide();
                var topic_name = $("#add-topic #topic_name").val();
                var image_data = $("#add-topic #image_data").val();

                $.ajax({
                    url: "{{ url('topics') }}",
                    type: "POST",
                    data: {
                        topic_name: topic_name,
                        image_data: image_data,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(resp){
                        if(resp == "1")
                        {
                            swal({
                                title: "Added!",
                                text: "Topics Added Successfully.",
                                type: "success",
                                showConfirmButton: true,
                                // timer: 1000,
                            },
                            function () {

                                $("#myModal").modal("hide");
                                /*$("#add-topic #topic_name").val("");
                                $("#add-topic #image_data").val("");
                                $('#imagePreview').css('background-image', 'url(http://i.pravatar.cc/500?img=7)');
                                // oTable.draw();*/
                                window.location.reload();
                            });
                        }
                        else
                        {
                            swal({
                                title: "Sorry!",
                                text: "Unable to add this inerest. Please try again later.",
                                type: "error",
                                closeOnConfirm: false,
                                timer: 1000,
                            }, 
                            function () {

                                $("#myModal").modal("hide");
                                $("#add-topic #topic_name").val("");
                                $("#add-topic #image_data").val("");
                                $('#imagePreview').css('background-image', 'url(http://i.pravatar.cc/500?img=7)');
                            });
                        }
                    }
                });
            }
            else
            {
                $(".form-error").show();
            }
        });

        $(document).on("click", ".edit-interest", function(){

            // alert("hello");
            var uid = $(this).data("uid");
            var interest = $(this).data("val");

            $("#edit-interest #interest_name").val(interest);
            $("#edit-interest #unique_id").val(uid);
            $(".error-interest_name").hide();
            $("#myModal-edit").modal("show");
        });

        $(".edit-interest-btn").click(function(){

            // alert("hello");
            if($("#edit-interest #interest_name").val() != "")
            {
                $(".error-interest_name").hide();
                var interest_name = $("#edit-interest #interest_name").val();
                var unique_id = $("#edit-interest #unique_id").val();

                $.ajax({
                    url: "{{ url('interest') }}/"+unique_id,
                    type: "POST",
                    data: {
                        interest_name   : interest_name,
                        _method: "PATCH"
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(resp){
                        if(resp == "1")
                        {
                            swal({
                                title: "Updated!",
                                text: "Interest Updated Successfully.",
                                type: "success",
                                showConfirmButton: true,
                                // timer: 1000,
                            },
                            function () {

                                $("#myModal-edit").modal("hide");
                                oTable.draw();
                            });
                        }
                        else
                        {
                            swal({
                                title: "Sorry!",
                                text: "Unable to add this inerest. Please try again later.",
                                type: "error",
                                closeOnConfirm: false,
                                timer: 1000,
                            }, 
                            function () {

                                $("#myModal-edit").modal("hide");
                                // oTable.draw();
                            });
                        }
                    }
                });
            }
            else
            {
                $(".error-interest_name").show();
            }
        });

        $(document).on("click", ".delete-interest", function(){

            var uid = $(this).data("uid");

            if(uid != ""){

                swal({
                    title: "Are you sure?",
                    text: "You really want to delete this Interest..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){

                        $.ajax({
                            url: "{{ url('interest') }}/"+uid,
                            type: "POST",
                            data: {
                                _method: "DELETE"
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(resp){
                                if(resp == "1")
                                {
                                    swal({
                                        title: "Deleted!",
                                        text: "Interest Deleted Successfully.",
                                        type: "success",
                                        showConfirmButton: true,
                                        // timer: 1000,
                                    },
                                    function () {

                                        // $("#myModal-edit").modal("hide");
                                        oTable.draw();
                                    });
                                }
                                else
                                {
                                    swal({
                                        title: "Sorry!",
                                        text: "Unable to add this inerest. Please try again later.",
                                        type: "error",
                                        closeOnConfirm: true,
                                        // timer: 1000,
                                    }, 
                                    function () {

                                        $("#myModal-edit").modal("hide");
                                        // oTable.draw();
                                    });
                                }
                            }
                        });

                });
            }
            else
            {
                // $(".error-interest_name").show();
                alert("interest not found")
            }
        });
    });
</script>

<script type="text/javascript">
            $(".imgAdd").click(function() {
                $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-4 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
            });
            $(document).on("click", "i.del", function() {
                $(this).parent().remove();
            });
            $(function() {
                $(document).on("change", ".uploadFile", function() {
                    var uploadFile = $(this);
                    var files = !!this.files ? this.files : [];
                    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            
                    if (/^image/.test(files[0].type)) { // only image file
                        var reader = new FileReader(); // instance of the FileReader
                        reader.readAsDataURL(files[0]); // read the local file
            
                        reader.onloadend = function() { // set image data as background of div
                            //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                            uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url(" + this.result + ")");
                        }
                    }
            
                });
            });
         </script>
         <script>
            $(function () {
            
            //var sourceDataObj = jQuery.parseJSON( "https://jsonplaceholder.typicode.com/comments" );
            
            //THIS SMALL SAMPLE WORKS 
            var sourceDataObj = [{"id": "1","value": "Adventure"},{"id": "2","value": "Advice" },{"id": "3","value": "Arts"},{"id": "4","value": "Beer" } ];
            
            
            $('#search').tokenfield({
                autocomplete: {
                    source: sourceDataObj,
                    delay: 100
            },
                showAutocompleteOnFocus: true
            });
            
            $('#search')
            .on('tokenfield:createtoken', function (event) {
                var existingTokens = $(this).tokenfield('getTokens');
                var exists = true;
                //PREVENT DUPLICATION
                $.each(existingTokens, function(index, token) {
                    if (token.value === event.attrs.value)
                        event.preventDefault();
                });
                //ALLOW ONLY TOKENS FROM SOURCE
                $.each(sourceDataObj, function(index, token) {
                    if (token.value === event.attrs.value)
                        exists = false;
                });
                if(exists === true)
                    event.preventDefault();
            })
            
            .on('tokenfield:removedtoken', function (event) {
                    alert('Token ' + event.attrs.value + ' removed!')
            })           
            
            
            });
         </script>
         
         <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>


@endsection