<!DOCTYPE html>
<html>
<head>
	<title>Recover Forgot Password</title>
</head>
<body>

	<p>Hi {{ $name }},</p>
	<br/>
	<br/>
	<p>Your request of forgot password is successfully completed.</p>
	<br/>
	<p>So, now you can login with your new password  <b>{{ $password }}</b></p>
	<br/>
	<br/>
	<p>Thank you,</p>
	<p><b>Team UHOO</b></p>

</body>
</html> 