@extends('layouts.master')

@section('custom_css')
<style type="text/css">

    .table
    {
        width: 100% !important;
        min-width: 0px;
    }

</style>

@endsection

@section('content') 

<div class="breadcrumb_sec">
    <ul class="breadcrumb">
        <li class="active msg">
            <a class="active_bread" href="b2c.html">B2C Customer List</a>
        </li>
    </ul>
</div>  
<section class="table_design padding_inside">
    <table class="customer-list display joined_data">
        <thead>
            <tr>
                <th>S. No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Active</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

            {{-- <tr>
                <td>Ankit Thakur</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Pawan Thakur</td>
                <td>dfs@gmail.com</td>
                <td>0987564321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Vikas Thakur</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Amandeep Kaur</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>

            <tr>
                <td>Manju Rana</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>

            <tr>
                <td>Pooja Sinha</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>

            <tr>
                <td>Sonia Sinha</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>

            <tr>
                <td>Sanjeev Thakur</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>
                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr>

            <tr>
                <td>Amandeep Thakur</td>
                <td>abc@gmail.com</td>
                <td>0987654321</td>
                <td>USA</td>
                <td>Yes</td>

                <td style="width: 150px;">
                    <a href="b2c-user.html" class="btn btn_view a-btn-slide-text1">
                        <span><strong>View</strong></span>
                    </a>
                </td>
            </tr> --}}

        </tbody>
    </table>
</section>

@endsection

@section('custom_js')

<script>

    $(document).ready(function() {
        
        oTable = $('.customer-list').DataTable({

            // dom: 'Blfrtip',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            processing: true,
            serverSide: true,
            filter:true,
            ajax: {
                url: '{{ url('b2c/list') }}',
                /*data: function(d){
                    d.company_id = $('#company_id').val();
                    d.project_id = $('#project_id').val();
                    d.search = {value:$('#search').val()}
                }*/
            },
            columns: [
                { data: 's_no', name: 's_no', sortable: false },
                { data: 'name', name: 'name', sortable: false },
                { data: 'email', name: 'email', sortable: false },
                { data: 'phone', name: 'phone', sortable: false },
                { data: 'location', name: 'location', sortable: false },
                { data: 'is_active', name: 'is_active', sortable: false },
                { data: 'action', name: 'action', sortable: false }
            ]
        });

        $(".owl-carousel.slider2").owlCarousel({
            loop: true,
            autoplay:true,
            dots: false,
            nav: true,
            margin:10,
            responsive: {
                320: {
                    items: 1
                },
                375: {
                    items: 1
                },
                425: {
                    items: 1
                },
                480: {
                    items: 1
                },
                600: {
                    items: 1
                },
                992: {
                    items: 2
                }
            } 
        }); 

        $(".owl-carousel").owlCarousel({
            loop: true,
            autoplay:true,
            dots: false,
            nav: true,
            margin:10,
            responsive: {
                320: {
                    items: 1
                },
                375: {
                    items: 1
                },
                425: {
                    items: 1
                },
                480: {
                    items: 1
                },
                600: {
                    items: 2
                },
                992: {
                    items: 4
                }
            }
        }); 

        $(".navigation > ul > li.dropdown ul").addClass("show");
    });
</script>

@endsection