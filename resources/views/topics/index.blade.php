@extends('layouts.master')

@section('custom_css')
<style type="text/css">
    .table
    {
        width: 100% !important;
        min-width: 0px;
    }

    #myDiv
    {
        text-align: left;
    }

    .form-error
    {
        font-size: 14px;
        padding-top: 6px;
        margin: 0px;
        font-weight: normal !important;
        color: #ff0000 !important;
    }

    .padding-0
    {
        padding: 0px;
    }

    .bkgrounds
    {
        padding-top: 0px;
        padding-bottom: 0px;
    }

    .topic-block
    {
        padding: 15px;
    }

    .topic-sect
    {
        text-decoration: none !important;
    }

    .form-error
    {
        font-size: 14px;
        padding-top: 6px;
        margin: 0px;
        font-weight: normal !important;
        color: #ff0000 !important;
        text-align: center;
    }
</style>

<!-- Sweet Alert -->
<link rel="stylesheet" href="{{ url('assets/css/sweetalert.css')  }}">

@endsection

@section('content') 

<div class="row full">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="breadcrumb_sec">
            <ul class="breadcrumb topic">
                <li class="active msg"><a class="inactive_bread" href="topic.html">Topic</a> </li>
            </ul>
        </div>
        <section class="interst_sec pading_bottom">
            <div class="future-apointmnt bottom_top Medication">
                <div class="patient_insight data_pading"></div>
                <button type="button" class="btn btn-primary book_now" data-toggle="modal" data-target="#myModal">Add New </button>
            </div>
            <div class="bkgrounds">
                <div class="row">
                    @foreach($topics as $topic)
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="{{ url('storage/app') }}/{{ $topic->filename }}" alt="{{ ucwords($topic->topic_name) }}"></p>
                                <span>{{ ucwords($topic->topic_name) }}</span>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    {{-- <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art6.png" alt="card image"></p>
                                <span>Math</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art7.png" alt="card image"></p>
                                <span>Film</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art8.png" alt="card image"></p>
                                <span>Technology</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art9.png" alt="card image"></p>
                                <span>Code</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art10.png" alt="card image"></p>
                                <span>Adventure</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art11.png" alt="card image"></p>
                                <span>Business</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3 padding-0 topic-block">
                        <a href="javascript:void(0);" class="topic-sect">
                            <div class="intest_img text-center">
                                <p><img class=" img-fluid" src="assets/images/art12.png" alt="card image"></p>
                                <span>Idea!</span>
                            </div>
                        </a>
                    </div> --}}
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal" id="myModal">
    <div class="modal-dialog interest">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Topic</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-error" style="display: none;">Please complete the form first</div>
                <form class="my-model" id="add-topic">
                    <div class="form-row upload_imgs interest">
                        <div class="form-group col-md-12 img">
                            <p>Upload Topic Image</p>
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload"  accept=".png, .jpg, .jpeg" />
                                    <input type='hidden' id="image_data" name="image_data" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="inputSelectGp">Topic Name</label>
                            <input type="text" class="form-control" placeholder="Topic Name" id="topic_name"name="topic_name">
                        </div>
                        <div class="form-group btnGrp col-md-12">
                            <button type="button" class="btn btn-primary add-topic-btn">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

{{-- Sweet Alert --}}
<script src="{{ url('assets/js/sweetalert.min.js') }}"></script>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
                $('#image_data').val(e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });


    $(document).ready(function() {
            
        /*oTable = $('.interest-list').DataTable({

            // dom: 'Blfrtip',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            processing: true,
            serverSide: true,
            filter:true,
            ajax: {
                url: '{{ url('interest/list') }}',
                // data: function(d){
                //     d.company_id = $('#company_id').val();
                //     d.project_id = $('#project_id').val();
                //     d.search = {value:$('#search').val()}
                // }
            },
            columns: [
                { data: 's_no', name: 's_no', sortable: false },
                { data: 'interest_name', name: 'interest_name', sortable: false },
                { data: 'is_active', name: 'is_active', sortable: false },
                { data: 'action', name: 'action', sortable: false }
            ]
        });

        $('#search').keyup(function(){
            oTable.draw();
        });*/

        $(".add-topic-btn").click(function(){

            // alert("hello");
            if($("#add-topic #topic_name").val() != "" && $("#add-topic #image_data").val() != "")
            {
                $(".form-error").hide();
                var topic_name = $("#add-topic #topic_name").val();
                var image_data = $("#add-topic #image_data").val();

                $.ajax({
                    url: "{{ url('topics') }}",
                    type: "POST",
                    data: {
                        topic_name: topic_name,
                        image_data: image_data,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(resp){
                        if(resp == "1")
                        {
                            swal({
                                title: "Added!",
                                text: "Topics Added Successfully.",
                                type: "success",
                                showConfirmButton: true,
                                // timer: 1000,
                            },
                            function () {

                                $("#myModal").modal("hide");
                                /*$("#add-topic #topic_name").val("");
                                $("#add-topic #image_data").val("");
                                $('#imagePreview').css('background-image', 'url(http://i.pravatar.cc/500?img=7)');
                                // oTable.draw();*/
                                window.location.reload();
                            });
                        }
                        else
                        {
                            swal({
                                title: "Sorry!",
                                text: "Unable to add this inerest. Please try again later.",
                                type: "error",
                                closeOnConfirm: false,
                                timer: 1000,
                            }, 
                            function () {

                                $("#myModal").modal("hide");
                                $("#add-topic #topic_name").val("");
                                $("#add-topic #image_data").val("");
                                $('#imagePreview').css('background-image', 'url(http://i.pravatar.cc/500?img=7)');
                            });
                        }
                    }
                });
            }
            else
            {
                $(".form-error").show();
            }
        });

        $(document).on("click", ".edit-interest", function(){

            // alert("hello");
            var uid = $(this).data("uid");
            var interest = $(this).data("val");

            $("#edit-interest #interest_name").val(interest);
            $("#edit-interest #unique_id").val(uid);
            $(".error-interest_name").hide();
            $("#myModal-edit").modal("show");
        });

        $(".edit-interest-btn").click(function(){

            // alert("hello");
            if($("#edit-interest #interest_name").val() != "")
            {
                $(".error-interest_name").hide();
                var interest_name = $("#edit-interest #interest_name").val();
                var unique_id = $("#edit-interest #unique_id").val();

                $.ajax({
                    url: "{{ url('interest') }}/"+unique_id,
                    type: "POST",
                    data: {
                        interest_name   : interest_name,
                        _method: "PATCH"
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(resp){
                        if(resp == "1")
                        {
                            swal({
                                title: "Updated!",
                                text: "Interest Updated Successfully.",
                                type: "success",
                                showConfirmButton: true,
                                // timer: 1000,
                            },
                            function () {

                                $("#myModal-edit").modal("hide");
                                oTable.draw();
                            });
                        }
                        else
                        {
                            swal({
                                title: "Sorry!",
                                text: "Unable to add this inerest. Please try again later.",
                                type: "error",
                                closeOnConfirm: false,
                                timer: 1000,
                            }, 
                            function () {

                                $("#myModal-edit").modal("hide");
                                // oTable.draw();
                            });
                        }
                    }
                });
            }
            else
            {
                $(".error-interest_name").show();
            }
        });

        $(document).on("click", ".delete-interest", function(){

            var uid = $(this).data("uid");

            if(uid != ""){

                swal({
                    title: "Are you sure?",
                    text: "You really want to delete this Interest..!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){

                        $.ajax({
                            url: "{{ url('interest') }}/"+uid,
                            type: "POST",
                            data: {
                                _method: "DELETE"
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(resp){
                                if(resp == "1")
                                {
                                    swal({
                                        title: "Deleted!",
                                        text: "Interest Deleted Successfully.",
                                        type: "success",
                                        showConfirmButton: true,
                                        // timer: 1000,
                                    },
                                    function () {

                                        // $("#myModal-edit").modal("hide");
                                        oTable.draw();
                                    });
                                }
                                else
                                {
                                    swal({
                                        title: "Sorry!",
                                        text: "Unable to add this inerest. Please try again later.",
                                        type: "error",
                                        closeOnConfirm: true,
                                        // timer: 1000,
                                    }, 
                                    function () {

                                        $("#myModal-edit").modal("hide");
                                        // oTable.draw();
                                    });
                                }
                            }
                        });

                });
            }
            else
            {
                // $(".error-interest_name").show();
                alert("interest not found")
            }
        });
    });
</script>

@endsection